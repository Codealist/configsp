<?php
namespace App\Services\Config\DependencyInjection;

use App\Services\Config\FileResolver;
use Pimple\Container;
use Pimple\ServiceProviderInterface;

class ConfigServiceProvider implements ServiceProviderInterface
{
    /**
     * @param Container $app
     */
    public function register(Container $app)
    {
        $app['config_loader'] = function($app) {
            return new FileResolver($app['debug'], $app['config.files']);
        };

        $app['config.files'] = [];
    }

}