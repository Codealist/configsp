<?php
namespace App\Services\Config;

use Silex\Application;

interface ConfigClient
{
    /**
     * @param string $directory
     * @param string $pattern
     * @return Application
     */
    public function loadFromDir($directory, $pattern);
}