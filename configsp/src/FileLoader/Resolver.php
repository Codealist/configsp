<?php

namespace App\Services\Config\FileLoader;

use Symfony\Component\Config\FileLocatorInterface;

class Resolver
{
    const EXT_YML = 'yml';
    const EXT_XML = 'xml';
    const EXT_INI = 'ini';

    protected $locator;


    public function __construct(FileLocatorInterface $locator)
    {
        $this->locator = $locator;
    }


    public function getLoader($filePath)
    {
        $extension = pathinfo($filePath, PATHINFO_EXTENSION);
        switch ($extension){
            case self::EXT_YML:
                return new YamlFileLoader($this->locator);
                break;
            default:
                throw new \InvalidArgumentException(sprintf("Wrong format (%s) of file %s",
                    pathinfo($filePath, PATHINFO_EXTENSION),
                    pathinfo($filePath, PATHINFO_BASENAME)
                ));
        }
    }


    function __destruct()
    {
        unset($this->locator);
    }


}