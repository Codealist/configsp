<?php
namespace Tests\Services\Config\FileLoader;

use App\Services\Config\FileLoader\Resolver;
use App\Services\Config\FileLoader\YamlFileLoader;
use Symfony\Component\Config\FileLocator;

class ResolverTest extends \PHPUnit_Framework_TestCase
{
    protected $locator;

    public function setUp()
    {
        $this->locator =
            $this
                ->getMockBuilder(FileLocator::class)
                ->disableOriginalConstructor()
                ->getMock();
    }

    /**
     * Test for exception throwing
     */
    public function testInvalidArgument()
    {
        $resolver = new Resolver($this->locator);
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessageRegExp("@.*Wrong format.*@");
        $resolver->getLoader(__DIR__."/mockfile.wrong");
    }

    /**
     * @dataProvider formatsProvider
     * @param string $extension
     * @param string $expected
     */
    public function testReturnAppropriateInstance($extension, $expected)
    {
        $resolver = new Resolver($this->locator);
        $generated = $resolver->getLoader(__DIR__."/mockfile.{$extension}");
        $this->assertInstanceOf($expected, $generated);
    }
    
    public function formatsProvider()
    {
        return [
            ['yml', YamlFileLoader::class]
        ];
    }
}
